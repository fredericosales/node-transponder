#ifndef _BASE_H_
#define _BASE_H_

/*
 * Frederico Sales
 * <frederico.sales@engenharia.ufjf.br>
 * PPGCC - ufjf
 * 2019
 *
 */

// include
#include <Arduino.h>
#include <TimeLib.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "config.h"


// object


// prototypes


// function


#endif
