#ifndef _CONFIG_H_
#define _CONFIG_H_
/*
 * Frederico Sales
 * <frederico.sales@engenharia.ufjf.br>
 * PPGCC - ufjf
 * 2019
 *
 */

// define
#define GPS_RX 13       // SoftwareSerial RX
#define GPS_TX 15       // SoftwareSerial TX
#define ATT 25          // delay value
#define SPD 38400        // GTPA010
#define LED 2           // led to indicate GPS 3D fix
#define DEBUG 0			//

// NodeMCU pinout Righ hand
#define D0 16           // GPIO16 USER  WAKE
#define D1 5            // GPIO5
#define D2 4            // GPIO4
#define D3 0            // GPIO0  FLASH
#define D4 2            // GPIO2  TXDi
#define D5 14           // GPIO14
#define D6 12           // GPIO12
#define D7 13           // GPIO13 RXD2
#define D8 15           // GPIO15 TXD2
#define RX 3            // GPIO13 RXDo
#define TX 1            // GPIO1  TXDo

// NodeMCU pinout left hand
// #define A0 ADC0		// ADC0 TOUT
#define S3 10           // GPIO10
#define S2 9            // GPIO9


#endif