#ifndef _TRANSPONDER_H_
#define _TRANSPONDER_H_

/*
 * Frederico Sales
 * <frederico.sales@engenharia.ufjf.br>
 * PPGCC - ufjf
 * 2019
 *
 */

// include
#include <Arduino.h>
#include <TimeLib.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "config.h"


// object
TinyGPSPlus gps;
SoftwareSerial Serial_GPS(GPS_RX, SW_SERIAL_UNUSED_PIN);

// prototypes
void bla(void);
void setSerial(void);
void doSomeMagic(int);

// function
void bla() {
  Serial.print("lat: ");
  Serial.print(gps.location.lat(), 6);
  Serial.print("\tlon: ");
  Serial.print(gps.location.lng(), 6);
  Serial.print("\talt: ");
  Serial.print(gps.altitude.meters(), 4);
  Serial.print("\tsat: ");
  Serial.println(gps.satellites.value());
  if(gps.satellites.value() > 3) {
    digitalWrite(LED, LOW);
  }
}

void setSerial() {
  // put your setup code here, to run once:
  pinMode(LED, OUTPUT);
  Serial.begin(SPD);
  Serial_GPS.begin(SPD);
  Serial.println(F("Lat, Lon, alt and sat."));
  Serial.println(F("GPS GY-NEO6MV2"));
  Serial.print(F("TinyGPS++ v. "));
  Serial.println(TinyGPSPlus::libraryVersion());
  Serial.println();
}

void doSomeMagic(int debug) {
  if(debug == "false") {
    while(Serial_GPS.available() > 0) {
      if(gps.encode(Serial_GPS.read())) {
          bla();
       }
    }
  } else if(debug == "true") {
    // use debug = true to see raw data
    while(Serial_GPS.available() > 0) {
      Serial.write(Serial_GPS.read());
    }
  }
  delay(ATT);
}

#endif
