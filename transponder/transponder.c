/*
 * Frederico Sales
 * <frederico.sales@engenharia.ufjf.br>
 * PPGCC - ufjf
 * 2019
 *
 */

// include 
#include "transponder.h"


// setup
void setup() {
	// do some conf stuff
	setSerial();
}


// loop 
void loop() {
	// white(true) { ... }
	soSomeMagic(DEBUG);
}